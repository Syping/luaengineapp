#!/usr/bin/env luaengine
--[[
******************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2020 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
******************************************************************************
--]]

local stackSwitch
local mainWindow
local itemStack = {}

function main()
    -- Window
    mainWindow = createMainWindow("LE Test Engine")
    local mainLayout = createLayout(VerticalLayout, mainWindow)

    -- Menu
    local menuBar = createMenuBar(mainWindow)
    local menuFile = createMenu("&File", menuBar)
    local menuEntryExit = createMenuEntry("&Exit", menuFile, "Alt+F4")
    connect(menuEntryExit, "triggered()", "testEngineClose")
    local menuHelp = createMenu("&Help", menuBar)
    local menuEntryAbout = createMenuEntry("&About LE Test Engine", menuHelp, "Ctrl+P")
    connect(menuEntryAbout, "triggered()", "testEngineAboutBox")

    -- Toolbar
    local toolBar = createToolBar("Toolbar", mainWindow)
    setWidgetImageSize(toolBar, 24, 24)
    local leftButton = createMenuEntry("Left", toolBar)
    setObjectImage(leftButton, "L.png")
    local rightButton = createMenuEntry("Right", toolBar)
    setObjectImage(rightButton, "R.png")

    -- Tabbar
    local tabBar = createTabBar(mainLayout)

    -- Main Tab
    local groupBox = createGroupBox("Buttons in Layouts", tabBar)
    addWidgetAsTab(groupBox, "Main Tab", tabBar)
    local groupBoxLayout1 = createLayout(HorizontalLayout, groupBox)
    local groupBoxLayout2 = createLayout(VerticalLayout, groupBoxLayout1)
    local testButton1 = createPushButton("Test 1", groupBoxLayout2)
    setObjectImage(testButton1, "L.png")
    local testButton2 = createPushButton("Test 2", groupBoxLayout2)
    local testButton3 = createPushButton("Test 3", groupBoxLayout1)
    local testButton4 = createPushButton("Test 4", groupBoxLayout1)
    local groupBoxLayout3 = createLayout(VerticalLayout, groupBoxLayout1)
    local testButton5 = createPushButton("Test 5", groupBoxLayout3)
    setObjectImage(testButton5, "R.png")
    local testButton6 = createPushButton("Test 6", groupBoxLayout3)

    -- Secondary Tab
    local secondaryTab = createWidgetTab("Secondary Tab", tabBar)
    local secondaryTabLayout = createLayout(VerticalLayout, secondaryTab)
    createLabel("<b>The Secondary Tab is here!</b>", secondaryTabLayout)
    local imagesLayout = createLayout(HorizontalLayout, secondaryTabLayout)
    createSpacerItem(SizePolicyExpanding, SizePolicyMinimum, imagesLayout)
    local leftImage = createLabel("!image:L.png", imagesLayout, 2)
    createLabel("Left", imagesLayout)
    createSpacerItem(SizePolicyExpanding, SizePolicyMinimum, imagesLayout)
    local rightImage = createLabel("!image:R.png", imagesLayout, 2)
    createLabel("Right", imagesLayout)
    createSpacerItem(SizePolicyExpanding, SizePolicyMinimum, imagesLayout)

    -- Third Tab
    local thirdTab = createWidgetTab("Third Tab", tabBar)
    local thirdTabLayout = createLayout(VerticalLayout, thirdTab)
    createLabel("<b>The Third Tab is here!</b>", thirdTabLayout)

    -- Last Tab
    local lastTab = createWidgetTab("Last Tab", tabBar)
    local lastTabLayout = createLayout(HorizontalLayout, lastTab)
    local listView = createListView(lastTabLayout)
    setWidgetImageSize(listView, 24, 24)
    setWidgetSizePolicy(listView, SizePolicyPreferred, SizePolicyExpanding)

    -- Last Tab: Upper Item
    local upperItem = createListItem("Upper", listView)
    setObjectImage(upperItem, "U.png")
    connect(upperItem, "selected()", "itemSelected")

    -- Last Tab: Bottom Item
    local bottomItem = createListItem("Bottom", listView)
    setObjectImage(bottomItem, "B.png")
    connect(bottomItem, "selected()", "itemSelected")

    -- Last Tab: Stack Switch
    stackSwitch = createStackSwitch(lastTabLayout)

    -- Stack Switch: Upper Stack
    local upperStack = createWidgetStack(stackSwitch)
    local upperLayout = createLayout(VerticalLayout, upperStack)
    createSpacerItem(SizePolicyMinimum, SizePolicyExpanding, upperLayout)
    local upperLabel = createLabel("<b>Upper Label is here!</b>", upperLayout)
    local upperImage = createLabel("!image:U.png", upperLayout, 2)
    createSpacerItem(SizePolicyMinimum, SizePolicyExpanding, upperLayout)
    itemStack[upperItem] = upperStack

    -- Stack Switch: Bottom Stack
    local bottomStack = createWidgetStack(stackSwitch)
    local bottomLayout = createLayout(VerticalLayout, bottomStack)
    createSpacerItem(SizePolicyMinimum, SizePolicyExpanding, bottomLayout)
    local bottomLabel = createLabel("<b>Bottom Label is here!</b>", bottomLayout)
    local bottomImage = createLabel("!image:B.png", bottomLayout, 2)
    createSpacerItem(SizePolicyMinimum, SizePolicyExpanding, bottomLayout)
    itemStack[bottomItem] = bottomStack

    -- Show Window
    setWidgetSize(mainWindow, 650, 450)
    showWidget(mainWindow, ShowDefault)
    return GuiExecuted
end

function itemSelected(item)
    setCurrentStack(itemStack[item], stackSwitch)
end

function testEngineAboutBox()
    -- Dialog
    local dialog = createDialog("About LE Test Engine", mainWindow)
    local dialogLayout = createLayout(VerticalLayout, dialog)

    -- Dialog Label
    local dialogLabel = createLabel("<h4>LE Test Engine</h4>Lua Engine Testing Ground", dialogLayout)

    -- Button Layout
    local buttonLayout = createLayout(HorizontalLayout, dialogLayout)
    createSpacerItem(SizePolicyExpanding, SizePolicyMinimum, buttonLayout)
    local dialogButton = createPushButton("&OK", buttonLayout)
    setObjectImage(dialogButton, "OK.png")
    connect(dialogButton, "clicked()", "closeDialog")

    -- Show Dialog
    setWidgetFixed(dialog)
    executeWidget(dialog)
    delete(dialog, DeleteInstant) -- preventing memory leaks
end

function testEngineClose()
    closeWidget(mainWindow)
end

function closeDialog(pushButton)
    disconnect(pushButton, "clicked()")
    closeWidget(getObjectWindow(pushButton))
end
