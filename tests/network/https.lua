#!/usr/bin/env luaengine
--[[
******************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2020 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
******************************************************************************
--]]

local globalLabel

function main()
    -- Window
    mainWindow = createMainWindow("LE TCP Test")
    local mainLayout = createLayout(VerticalLayout, mainWindow)

    local socket = createTcpSocket()
    connect(socket, "connected()", "socketConnected")
    connect(socket, "readyRead()", "socketReadyRead")
    socketConnect(socket, "syping.de:443")

    globalLabel = createLabel("<b>Connecting...</b>", mainLayout)

    -- Show Window
    setWidgetSize(mainWindow, 650, 450)
    showWidget(mainWindow, ShowDefault)
    return GuiExecuted
end

function socketConnected(socket)
    setObjectText(globalLabel, "<b>Connected!</b>")
    tcpSocketStartTLS(socket)
    eIOWrite(socket, "GET / HTTP/1.1\r\nHost: www.syping.de\r\nUser-Agent: LuaEngine\r\nConnection: close\r\n\r\n")
    socketFlush(socket)
end

function socketReadyRead(socket)
    setObjectText(globalLabel, "<b>Reading...</b>")
    local readData = eIOReadAll(socket)
    setObjectText(globalLabel, readData)
end
