#/*****************************************************************************
#* luaEngine Lua Engine for Qt
#* Copyright (C) 2018-2020 Syping
#*
#* Licensed under the Apache License, Version 2.0 (the "License");
#* you may not use this file except in compliance with the License.
#* You may obtain a copy of the License at
#*
#*     http://www.apache.org/licenses/LICENSE-2.0
#*
#* Unless required by applicable law or agreed to in writing, software
#* distributed under the License is distributed on an "AS IS" BASIS,
#* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#* See the License for the specific language governing permissions and
#* limitations under the License.
#*****************************************************************************/

QT += network widgets
TARGET = luaengineapp
win32: TARGET = LuaEngineApp
CONFIG += c++11

static: DEFINES += LUAENGINE_STATIC

# PROJECT PREFIX
unix {
    isEmpty(LUAENGINE_PREFIX): LUAENGINE_PREFIX = /usr/local
    isEmpty(LUAENGINE_BINDIR): LUAENGINE_BINDIR = $$LUAENGINE_PREFIX/bin
    isEmpty(LUAENGINE_LIBDIR): LUAENGINE_LIBDIR = $$LUAENGINE_PREFIX/lib
}

unix: LIBS += "-Wl,-rpath,\'$$LUAENGINE_LIBDIR\'" -L$$OUT_PWD/../luaengineos -lLuaEngineOS -L$$OUT_PWD/../luaenginenetwork -lLuaEngineNetwork -L$$OUT_PWD/../luaengineio -lLuaEngineIO -L$$OUT_PWD/../luaenginegui -lLuaEngineGui -L$$OUT_PWD/../luaenginecore -lLuaEngine
CONFIG(debug, debug|release): win32: LIBS += -L$$OUT_PWD/../luaengineos/debug -lLuaEngineOS -L$$OUT_PWD/../luaenginenetwork/debug -lLuaEngineNetwork -L$$OUT_PWD/../luaengineio/debug -lLuaEngineIO -L$$OUT_PWD/../luaenginegui/debug -lLuaEngineGui -L$$OUT_PWD/../luaenginecore/debug -lLuaEngine
CONFIG(release, debug|release): win32: LIBS += -L$$OUT_PWD/../luaengineos/release -lLuaEngineOS -L$$OUT_PWD/../luaenginenetwork/release -lLuaEngineNetwork -L$$OUT_PWD/../luaengineio/release -lLuaEngineIO -L$$OUT_PWD/../luaenginegui/release -lLuaEngineGui -L$$OUT_PWD/../luaenginecore/release -lLuaEngine

INCLUDEPATH += \
    ../luaenginecore/luaengine \
    ../luaenginegui/luaengine \
    ../luaengineio/luaengine \
    ../luaenginenetwork/luaengine \
    ../luaengineos/luaengine

SOURCES += \
    main.cpp

OTHER_FILES += \
    app.lua \
    app.rc

include(../luaenginecore/lua_module.pri)

unix {
    target.path = $$LUAENGINE_BINDIR
    INSTALLS += target
}

win32: CONFIG(LUAENGINE_RESOURCE_SCRIPT) {
    DEFINES += LUAENGINE_RESOURCE_SCRIPT
    HEADERS += resource.h
    RC_FILE = app.rc
}
