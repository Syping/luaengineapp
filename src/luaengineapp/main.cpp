/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2018-2019 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#include "LuaEngineNetwork.h"
#include "LuaEngineGui.h"
#include "LuaEngineIO.h"
#include "LuaEngineOS.h"
#include <QApplication>
#include <QMessageBox>
#include <QFont>
#include <QFile>
#ifdef Q_OS_WIN
#include "windows.h"
#include "resource.h"
#endif

int main(int argc, char *argv[])
{
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling, true);
    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps, true);
    QApplication a(argc, argv);
#ifdef Q_OS_WIN
#if QT_VERSION >= QT_VERSION_CHECK(5, 4, 0)
    if (QSysInfo::windowsVersion() >= 0x0080)
    {
        a.setFont(QApplication::font("QMenu"));
    }
#endif
#endif

    QByteArray luaScript;
#ifdef Q_OS_WIN
#ifdef LUAENGINE_RESOURCE_SCRIPT
    {
        HMODULE handle = GetModuleHandleW(NULL);
        HRSRC resource = FindResourceW(handle, MAKEINTRESOURCE(IDR_SCRIPT1), L"SCRIPT");
        HGLOBAL resourceData = LoadResource(handle, resource);
        DWORD size = SizeofResource(handle, resource);
        const char *data = static_cast<const char*>(LockResource(resourceData));
        luaScript = QByteArray(data, size);
    }
#else
    {
        QFile executable(a.arguments().first());
        if (executable.open(QIODevice::ReadOnly)) {
            qint64 executableSize = executable.size();
            executable.seek(executableSize - 6);
            QByteArray footer = executable.read(6);
            if (footer.right(2) == QByteArray("\xb4\x00", 2)) {
                bool sizeOk;
                qint64 scriptSize = footer.left(4).toHex().toLongLong(&sizeOk, 16);
                if (sizeOk) {
                    executable.seek(executableSize - scriptSize - 6);
                    luaScript = executable.read(scriptSize);
                }
            }
            else {
                QMessageBox::information(NULL, "Lua Engine", "Lua Engine Script footer not found!");
            }
        }
    }
#endif
#endif

    LuaEngineGui luaEngineGui(LuaEngine::PortableEngineType);
    LuaEngineIO::pushClass(&luaEngineGui);
    LuaEngineNetwork::pushClass(&luaEngineGui);
    LuaEngineOS::pushClass(&luaEngineGui);
    luaEngineGui.pushVariant("_LuaEngineMode", "GUI");
    luaEngineGui.executeLuaScript(luaScript);

    QVariantList arguments;
    for (const QString &argument : a.arguments()) {
        arguments << QVariant::fromValue(argument);
    }

    if (luaEngineGui.executeLuaFunction("main", arguments, true)) {
        const QVariant variant = luaEngineGui.returnVariant();
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
        if (variant.typeId() == QMetaType::Int || variant.typeId() == QMetaType::LongLong) {
#else
        if (variant.type() == QVariant::Int || variant.type() == QVariant::LongLong) {
#endif
            return variant.toInt();
        }
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
        else if (variant.typeId() == QMetaType::QByteArray) {
#else
        else if (variant.type() == QVariant::ByteArray) {
#endif
            if (variant.toString() == "GuiExecuted" || variant.toString() == "CliExecuted") {
                return a.exec();
            }
            else {
                return 1;
            }
        }
        else {
            return 1;
        }
    }

    return 1;
}
