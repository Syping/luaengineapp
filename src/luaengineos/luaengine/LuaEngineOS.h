/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2019 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#ifndef LUAENGINEOS_H
#define LUAENGINEOS_H

#include "LuaEngineOS_global.h"
#include "LuaEngineAddon.h"
#include "LuaEngine.h"
#include <QString>
#include <QObject>

class LUAENGINEOSSHARED_EXPORT LuaEngineOS : public LuaEngineAddon
{
    Q_OBJECT
public:
    static void pushClass(lua_State *L_p);
    static void pushClass(LuaEngine *luaEngine);
    static int executeProcess(lua_State *L_p);

private:
    lua_State *L;
    static QString nameForPointer(void *pointer);
};

#endif // LUAENGINEOS_H
