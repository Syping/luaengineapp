/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2019 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#include "LuaEngineOS.h"
#include <QTextStream>
#include <QProcess>
#include <QString>
#include <QObject>

void LuaEngineOS::pushClass(lua_State *L_p)
{
    // Process
    pushFunction(L_p, "executeProcess", executeProcess);
}

void LuaEngineOS::pushClass(LuaEngine *luaEngine)
{
    pushClass(luaEngine->luaState());
}

int LuaEngineOS::executeProcess(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 1) {
        int processReturn = 0;
        bool runInBackground = false;
        bool processSuccessed = false;
        if (getArgumentCount(L_p) >= 2) {
            QStringList processArguments;
            QString processPath = getVariant(L_p, 1).toString();
            QVariant argument = getVariant(L_p, 2);
            if ((QMetaType::Type)argument.type() == QMetaType::QVariantMap) {
                QVariantMap argumentMap = argument.toMap();
                QVariantMap::const_iterator it = argumentMap.constBegin();
                QVariantMap::const_iterator end = argumentMap.constEnd();
                while (it != end) {
                    processArguments << it.value().toString();
                    it++;
                }
            }
            else if (argument.type() == QVariant::Bool) {
                runInBackground = argument.toBool();
            }
            else {
                processArguments << argument.toString();
            }
            if (getArgumentCount(L_p) >= 3) {
                if (argument.type() == QVariant::Bool) {
                    processArguments << argument.toString();
                }
                runInBackground = getVariant(L_p, 3).toBool();
            }
            if (runInBackground) {
                processSuccessed = QProcess::startDetached(processPath, processArguments);
            }
            else {
                processReturn = QProcess::execute(processPath, processArguments);
            }
        }
        else {
#if QT_VERSION >= 0x050F00
            processReturn = system(getVariant(L_p, 1).toString().toUtf8().constData());
#else
            processReturn = QProcess::execute(getVariant(L_p, 1).toString());
#endif
        }
        if (runInBackground && !processSuccessed) {
            processReturn = -2;
        }
        else if (!runInBackground && processReturn == 0) {
            processSuccessed = true;
        }
        pushVariant(L_p, processSuccessed);
        pushVariant(L_p, processReturn);
        return 2;
    }
    pushVariant(L_p, false);
    pushVariant(L_p, -2);
    return 2;
}

QString LuaEngineOS::nameForPointer(void *pointer)
{
    QString nameStorage;
    QTextStream(&nameStorage) << "LuaEngineOS" << pointer;
    return nameStorage;
}
