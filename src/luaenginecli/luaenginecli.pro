#/*****************************************************************************
#* luaEngine Lua Engine for Qt
#* Copyright (C) 2020 Syping
#*
#* Licensed under the Apache License, Version 2.0 (the "License");
#* you may not use this file except in compliance with the License.
#* You may obtain a copy of the License at
#*
#*     http://www.apache.org/licenses/LICENSE-2.0
#*
#* Unless required by applicable law or agreed to in writing, software
#* distributed under the License is distributed on an "AS IS" BASIS,
#* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#* See the License for the specific language governing permissions and
#* limitations under the License.
#*****************************************************************************/

QT += network
QT -= gui
TARGET = luaenginecli
CONFIG += c++11 \
    console
CONFIG -= app_bundle
VERSION = 0.1

static: DEFINES += LUAENGINE_STATIC

# PROJECT PREFIX
unix {
    isEmpty(LUAENGINE_PREFIX): LUAENGINE_PREFIX = /usr/local
    isEmpty(LUAENGINE_BINDIR): LUAENGINE_BINDIR = $$LUAENGINE_PREFIX/bin
    isEmpty(LUAENGINE_LIBDIR): LUAENGINE_LIBDIR = $$LUAENGINE_PREFIX/lib
}

unix: LIBS += "-Wl,-rpath,\'$$LUAENGINE_LIBDIR\'" -L$$OUT_PWD/../luaengineos -lLuaEngineOS -L$$OUT_PWD/../luaenginenetwork -lLuaEngineNetwork -L$$OUT_PWD/../luaengineio -lLuaEngineIO -L$$OUT_PWD/../luaenginecore -lLuaEngine
CONFIG(debug, debug|release): win32: LIBS += -L$$OUT_PWD/../luaengineos/debug -lLuaEngineOS -L$$OUT_PWD/../luaenginenetwork/debug -lLuaEngineNetwork -L$$OUT_PWD/../luaengineio/debug -lLuaEngineIO -L$$OUT_PWD/../luaenginecore/debug -lLuaEngine
CONFIG(release, debug|release): win32: LIBS += -L$$OUT_PWD/../luaengineos/release -lLuaEngineOS -L$$OUT_PWD/../luaenginenetwork/release -lLuaEngineNetwork -L$$OUT_PWD/../luaengineio/release -lLuaEngineIO -L$$OUT_PWD/../luaenginecore/release -lLuaEngine

INCLUDEPATH += \
    ../luaenginecore/luaengine \
    ../luaengineio/luaengine \
    ../luaenginenetwork/luaengine \
    ../luaengineos/luaengine

SOURCES += \
    main.cpp

include(../luaenginecore/lua_module.pri)

unix {
    target.path = $$LUAENGINE_BINDIR
    INSTALLS += target
}

win32 {
    RC_LANG = 0x0
    RC_ICONS = ../../res/lua.ico
    QMAKE_TARGET_COMPANY = "Syping"
    QMAKE_TARGET_DESCRIPTION = "LuaEngine CLI Runtime"
    QMAKE_TARGET_COPYRIGHT = "Copyright (c) 2020-2021 Syping"
    QMAKE_TARGET_PRODUCT = "luaenginecli"
}
