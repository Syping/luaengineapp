/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2018-2019 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#include "LuaEngineNetwork.h"
#include "LuaEngineIO.h"
#include "LuaEngineOS.h"
#include <QCoreApplication>
#include <QTextStream>
#include <QFile>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QVariantList arguments;
    const QStringList appArgs = a.arguments();
    for (const QString &argument : appArgs) {
        arguments << QVariant::fromValue(argument);
    }

    if (arguments.length() >= 2) {
        QVariant rtPath = arguments.first();
        arguments.removeFirst();
        QVariant scriptPath = arguments.first();
        QFile luaScript(scriptPath.toString());
        if (!luaScript.open(QIODevice::ReadOnly)) {
#if QT_VERSION >= 0x050F00
            QTextStream(stderr) << "Error: Failed to open \"" << arguments.first().toString() << "\"." << Qt::endl;
#else
            QTextStream(stderr) << "Error: Failed to open \"" << arguments.first().toString() << "\"." << endl;
#endif
            return 1;
        }

        if (QString::fromUtf8(luaScript.read(2)) == "#!") {
            luaScript.readLine();
        }
        else {
            luaScript.reset();
        }

        LuaEngine luaEngine(LuaEngine::RuntimeEngineType);
        LuaEngineIO::pushClass(&luaEngine);
        LuaEngineNetwork::pushClass(&luaEngine);
        LuaEngineOS::pushClass(&luaEngine);
        luaEngine.pushVariant("_LuaEngineRT", rtPath);
        luaEngine.pushVariant("_LuaEngineMode", "CLI");
        luaEngine.setProperty("ScriptPath", scriptPath);
        luaEngine.executeLuaScript(luaScript.readAll());

        if (luaEngine.executeLuaFunction("main", arguments, true)) {
            QVariant variant = luaEngine.returnVariant();
#if QT_VERSION >= 0x060000
            if (variant.typeId() == QMetaType::Int || variant.typeId() == QMetaType::LongLong) {
#else
            if (variant.type() == QVariant::Int || variant.type() == QVariant::LongLong) {
#endif
                return variant.toInt();
            }
#if QT_VERSION >= 0x060000
            else if (variant.typeId() == QMetaType::QByteArray) {
#else
            else if (variant.type() == QVariant::ByteArray) {
#endif
                if (variant.toString() == "CliExecuted") {
                    return a.exec();
                }
                else {
                    return 1;
                }
            }
            else {
                return 1;
            }
        }

        return 1;
    }
    else {
        return 1;
    }
}
