/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2018-2019 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#include "LuaEngineNetwork.h"
#include "LuaEngineGui.h"
#include "LuaEngineIO.h"
#include "LuaEngineOS.h"
#include <QApplication>
#include <QTextStream>
#include <QFont>
#include <QFile>

int main(int argc, char *argv[])
{
#if QT_VERSION >= 0x060000
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling, true);
    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps, true);
#endif
    QApplication a(argc, argv);

#ifdef Q_OS_WIN
#if QT_VERSION >= 0x050400
    if (QSysInfo::windowsVersion() >= 0x0080) {
        a.setFont(QApplication::font("QMenu"));
    }
#endif
#endif

    QVariantList arguments;
    const QStringList appArgs = a.arguments();
    for (const QString &argument : appArgs) {
        arguments << QVariant::fromValue(argument);
    }

    if (arguments.length() >= 2) {
        QVariant rtPath = arguments.first();
        arguments.removeFirst();
        QVariant scriptPath = arguments.first();
        QFile luaScript(scriptPath.toString());
        if (!luaScript.open(QIODevice::ReadOnly)) {
#if QT_VERSION >= 0x050F00
            QTextStream(stderr) << "Error: Failed to open \"" << arguments.first().toString() << "\"." << Qt::endl;
#else
            QTextStream(stderr) << "Error: Failed to open \"" << arguments.first().toString() << "\"." << endl;
#endif
            return 1;
        }

        if (QString::fromUtf8(luaScript.read(2)) == "#!") {
            luaScript.readLine();
        }
        else {
            luaScript.reset();
        }

        LuaEngineGui luaEngineGui(LuaEngine::RuntimeEngineType);
        LuaEngineIO::pushClass(&luaEngineGui);
        LuaEngineNetwork::pushClass(&luaEngineGui);
        LuaEngineOS::pushClass(&luaEngineGui);
        luaEngineGui.pushVariant("_LuaEngineRT", rtPath);
        luaEngineGui.pushVariant("_LuaEngineMode", "GUI");
        luaEngineGui.setProperty("ScriptPath", scriptPath);
        luaEngineGui.executeLuaScript(luaScript.readAll());

        if (luaEngineGui.executeLuaFunction("main", arguments, true)) {
            QVariant variant = luaEngineGui.returnVariant();
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
            if (variant.typeId() == QMetaType::Int || variant.typeId() == QMetaType::LongLong) {
#else
            if (variant.type() == QVariant::Int || variant.type() == QVariant::LongLong) {
#endif
                return variant.toInt();
            }
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
            else if (variant.typeId() == QMetaType::QByteArray) {
#else
            else if (variant.type() == QVariant::ByteArray) {
#endif
                if (variant.toString() == "GuiExecuted" || variant.toString() == "CliExecuted") {
                    return a.exec();
                }
                else {
                    return 1;
                }
            }
            else {
                return 1;
            }
        }

        return 1;
    }
    else {
        return 1;
    }
}
