/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2018-2019 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#include "LuaEngine.h"
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QCoreApplication>
#include <QTextStream>
#include <QSaveFile>
#include <QFile>
#include <QDir>

#if QT_VERSION >= 0x050F00
#define le_endl Qt::endl
#else
#define le_endl endl
#endif

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    a.setApplicationName("LuaEngine Compiler");
    a.setApplicationVersion("0.1");

    QCommandLineParser parser;

    QCommandLineOption pLuaEngineOption(QStringList() << "le" << "luaengine", "Location of portable LuaEngine executor.", "luaengine");
    parser.addOption(pLuaEngineOption);

    parser.addPositionalArgument("source", "Source file to build.");
    parser.addPositionalArgument("output", "Output file.");

    parser.addHelpOption();
    parser.addVersionOption();

    parser.process(a);

    QString inputFile;
    QString outputFile;
    const QStringList args = parser.positionalArguments();
    if (args.length() < 1) {
        QTextStream(stderr) << "Error: No input files." << le_endl;
        return 1;
    }
    else {
        inputFile = args.at(0);
    }
    if (args.length() < 2) {
        outputFile = QDir::current().absolutePath() + "/luaenginec.out";
    }
    else {
        outputFile = args.at(1);
    }

    QFile luaScript(inputFile);
    if (!luaScript.open(QIODevice::ReadOnly)) {
        QTextStream(stderr) << "Error: Failed to open \"" << inputFile << "\"." << le_endl;
        return 1;
    }

    if (QString::fromUtf8(luaScript.read(2)) == "#!") {
        luaScript.readLine();
    }
    else {
        luaScript.reset();
    }

    LuaEngine luaEngine;
    if (!luaEngine.loadLuaScript(luaScript.readAll())) {
        QTextStream(stderr) << "Error: Failed to load \"" << inputFile << "\"." << le_endl;
        return 1;
    }

    QSaveFile outputSaveFile(outputFile);
    if (!outputSaveFile.open(QIODevice::WriteOnly)) {
        QTextStream(stderr) << "Error: Failed to open \"" << outputFile << "\"." << le_endl;
        return 1;
    }

    QString pLuaEngine = parser.value(pLuaEngineOption);
    if (!pLuaEngine.isEmpty()) {
        QFile portableLuaEngine(pLuaEngine);
        if (!portableLuaEngine.open(QIODevice::ReadOnly)) {
            outputSaveFile.cancelWriting();
            outputSaveFile.commit();
            QTextStream(stderr) << "Error: Failed to open \"" << pLuaEngine << "\"." << le_endl;
            return 1;
        }
        outputSaveFile.write(portableLuaEngine.readAll());
    }

    const QByteArray luaDump = luaEngine.dumpLuaScript();
    int dumpSize = luaDump.size();
    outputSaveFile.write(luaDump);

    if (!pLuaEngine.isEmpty()) {
        QByteArray lengthArray = QByteArray::number(dumpSize, 16);
        if (lengthArray.size() > 8) {
            outputSaveFile.cancelWriting();
            outputSaveFile.commit();
            QTextStream(stderr) << "Error: Lua Engine script is too large." << le_endl;
            return 1;
        }
        else {
            while (lengthArray.size() != 8) {
                lengthArray.insert(0, "0");
            }
        }
        outputSaveFile.write(QByteArray::fromHex(lengthArray));
        outputSaveFile.write("\xb4\x00", 2);
    }

    if (!outputSaveFile.commit()) {
        QTextStream(stderr) << "Error: Failed to write \"" << args.at(1) << "\"." << le_endl;
        return 1;
    }

    return 0;
}
