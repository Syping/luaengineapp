#/*****************************************************************************
#* luaEngine Lua Engine for Qt
#* Copyright (C) 2018-2021 Syping
#*
#* Licensed under the Apache License, Version 2.0 (the "License");
#* you may not use this file except in compliance with the License.
#* You may obtain a copy of the License at
#*
#*     http://www.apache.org/licenses/LICENSE-2.0
#*
#* Unless required by applicable law or agreed to in writing, software
#* distributed under the License is distributed on an "AS IS" BASIS,
#* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#* See the License for the specific language governing permissions and
#* limitations under the License.
#*****************************************************************************/

QT -= gui
TARGET = LuaEngine
TEMPLATE = lib
CONFIG += c++11 \
    skip_target_version_ext
VERSION = 0.1

DEFINES += LUAENGINE_LIBRARY
static: DEFINES += LUAENGINE_STATIC

# PROJECT PREFIX
unix {
    isEmpty(LUAENGINE_PREFIX): LUAENGINE_PREFIX = /usr/local
    isEmpty(LUAENGINE_LIBDIR): LUAENGINE_LIBDIR = $$LUAENGINE_PREFIX/lib
}

SOURCES += \
    luaengine/LuaEngine.cpp \
    luaengine/LuaEngineRegistry.cpp

HEADERS += \
    luaengine/LuaEngine.h \
    luaengine/LuaEngine_global.h \
    luaengine/LuaEngine_macro.h \
    luaengine/LuaEngineAddon.h \
    luaengine/LuaEngineRegistry.h
	
include(lua.pri)
include(lua_module.pri)

unix {
    target.path = $$LUAENGINE_LIBDIR
    INSTALLS += target
}

win32 {
    RC_LANG = 0x0
    QMAKE_TARGET_COMPANY = "Syping"
    QMAKE_TARGET_DESCRIPTION = "LuaEngine Library"
    QMAKE_TARGET_COPYRIGHT = "Copyright (c) 2018-2021 Syping"
    QMAKE_TARGET_PRODUCT = "LuaEngine"
}
