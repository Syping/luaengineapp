/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2019 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#ifndef LUAENGINEREGISTRY_H
#define LUAENGINEREGISTRY_H

#include "LuaEngine_global.h"
#include <QObject>
#include <QMap>

class LUAENGINESHARED_EXPORT LuaEngineRegistry : public QObject
{
    Q_OBJECT
public:
    static LuaEngineRegistry* getInstance() { return &luaEngineRegistryInstance; }
    void registerEngine(void *state, void *engine);
    void unregisterEngine(void *state);
    void* getEngine(void *state);

private:
    static LuaEngineRegistry luaEngineRegistryInstance;
    QMap<void*, void*> registryMap;
};

#define engineRegistry LuaEngineRegistry::getInstance()

#endif // LUAENGINEREGISTRY_H
