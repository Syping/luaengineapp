/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2019 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#include "LuaEngineRegistry.h"

LuaEngineRegistry LuaEngineRegistry::luaEngineRegistryInstance;

void LuaEngineRegistry::registerEngine(void *state, void *engine)
{
    registryMap.insert(state, engine);
}

void LuaEngineRegistry::unregisterEngine(void *state)
{
    registryMap.remove(state);
}

void* LuaEngineRegistry::getEngine(void *state)
{
    return registryMap.value(state, nullptr);
}
