/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2018-2021 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#ifndef LUAENGINE_H
#define LUAENGINE_H

#include "LuaEngine_global.h"
#include <QVariantList>
#include <QByteArray>
#include <QIODevice>
#include <QVariant>
#include <QString>
#include <QObject>
extern "C" {
#include "lua_module.h"
}

class LUAENGINESHARED_EXPORT LuaEngine : public QObject
{
    Q_OBJECT
public:
    enum LuaEngineType : qint8 { RuntimeEngineType = 0, PortableEngineType = 1, UnknownEngineType = -1 };
    LuaEngine(QObject *parent = nullptr, bool loadBaseLibraries = true);
    LuaEngine(LuaEngineType engineType, QObject *parent = nullptr, bool loadBaseLibraries = true);
    ~LuaEngine();
    lua_State* luaState();
    LuaEngineType engineType();
    void loadBaseLibraries();
    QByteArray dumpLuaScript();
    bool loadLuaScript(const QByteArray &data);
    bool loadLuaScript(QIODevice *device, bool closeDevice = true);
    bool executeLuaScript(const QByteArray &data);
    bool executeLuaScript(QIODevice *device, bool closeDevice = true);
    bool executeLuaFunction(const char *name, bool requireReturn = false);
    static bool executeLuaFunction(lua_State *L_p, const char *name, bool requireReturn = false);
    bool executeLuaFunction(const char *name, const QVariant &argument, bool requireReturn = false);
    static bool executeLuaFunction(lua_State *L_p, const char *name, const QVariant &argument, bool requireReturn = false);
    bool executeLuaFunction(const char *name, const QVariantList &args, bool requireReturn = false);
    static bool executeLuaFunction(lua_State *L_p, const char *name, const QVariantList &args, bool requireReturn = false);
    void pushFunction(const char *name, lua_CFunction function);
    static void pushFunction(lua_State *L_p, const char *name, lua_CFunction function);
    void pushPointer(const char *name, void *pointer);
    static void pushPointer(lua_State *L_p, const char *name, void *pointer);
    void pushPointer(void *pointer);
    static void pushPointer(lua_State *L_p, void *pointer);
    void pushVariant(const char *name, const QVariant &variant);
    static void pushVariant(lua_State *L_p, const char *name, const QVariant &variant);
    void pushVariant(const QVariant &variant);
    static void pushVariant(lua_State *L_p, const QVariant &variant);
    QVariant getVariant(const char *name);
    static QVariant getVariant(lua_State *L_p, const char *name);
    QVariant getVariant(int index);
    static QVariant getVariant(lua_State *L_p, int index);
    void* returnPointer();
    static void* returnPointer(lua_State *L_p);
    void* getPointer(const char* name);
    static void* getPointer(lua_State *L_p, const char* name);
    void* getPointer(int index);
    static void* getPointer(lua_State *L_p, int index);
    QVariant returnVariant();
    static QVariant returnVariant(lua_State *L_p);
    static QVariantList getArguments(lua_State *L_p);
    static int getArgumentCount(lua_State *L_p);

private:
    lua_State *L;
    LuaEngineType p_engineType;
    static int luaEngineWriter_p(lua_State *L_p, const void *buffer, size_t size, void *array);
    static int luaEngineQuit_p(lua_State *L_p);
    static int luaEngineVersion_p(lua_State *L_p);
    static int luaEnginePlatform_p(lua_State *L_p);
    static int luaObjectDelete_p(lua_State *L_p);
    static int luaObjectGetParent_p(lua_State *L_p);
    static int luaObjectSetParent_p(lua_State *L_p);
    static int luaTriggerConnect_p(lua_State *L_p);
    static int luaTriggerDisconnect_p(lua_State *L_p);

private slots:
    void luaTriggerSlot_p();
};

#endif // LUAENGINE_H
