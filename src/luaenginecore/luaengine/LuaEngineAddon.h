/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2018-2020 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#ifndef LUAENGINEADDON_H
#define LUAENGINEADDON_H

#include "LuaEngine_global.h"
#include "LuaEngine.h"

class LUAENGINESHARED_EXPORT LuaEngineAddon : public QObject
{
    Q_OBJECT
public:
    static void pushFunction(lua_State *L_p, const char *name, lua_CFunction function) {
        LuaEngine::pushFunction(L_p, name, function);
    }
    static void pushPointer(lua_State *L_p, const char *name, void *pointer) {
        LuaEngine::pushPointer(L_p, name, pointer);
    }
    static void pushPointer(lua_State *L_p, void *pointer) {
        LuaEngine::pushPointer(L_p, pointer);
    }
    static void pushVariant(lua_State *L_p, const char *name, const QVariant &variant) {
        LuaEngine::pushVariant(L_p, name, variant);
    }
    static void pushVariant(lua_State *L_p, const QVariant &variant) {
        LuaEngine::pushVariant(L_p, variant);
    }
    static QVariant getVariant(lua_State *L_p, const char *name) {
        return LuaEngine::getVariant(L_p, name);
    }
    static QVariant getVariant(lua_State *L_p, int index) {
        return LuaEngine::getVariant(L_p, index);
    }
    static void* returnPointer(lua_State *L_p) {
        return LuaEngine::returnPointer(L_p);
    }
    static void* getPointer(lua_State *L_p, const char* name) {
        return LuaEngine::getPointer(L_p, name);
    }
    static void* getPointer(lua_State *L_p, int index) {
        return LuaEngine::getPointer(L_p, index);
    }
    static QVariant returnVariant(lua_State *L_p) {
        return LuaEngine::returnVariant(L_p);
    }
    static QVariantList getArguments(lua_State *L_p) {
        return LuaEngine::getArguments(L_p);
    }
    static int getArgumentCount(lua_State *L_p) {
        return LuaEngine::getArgumentCount(L_p);
    }

};

#endif // LUAENGINEADDON_H
