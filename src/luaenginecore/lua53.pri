#/*****************************************************************************
#* luaEngine Lua Engine for Qt
#* Copyright (C) 2020 Syping
#*
#* Licensed under the Apache License, Version 2.0 (the "License");
#* you may not use this file except in compliance with the License.
#* You may obtain a copy of the License at
#*
#*     http://www.apache.org/licenses/LICENSE-2.0
#*
#* Unless required by applicable law or agreed to in writing, software
#* distributed under the License is distributed on an "AS IS" BASIS,
#* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#* See the License for the specific language governing permissions and
#* limitations under the License.
#*****************************************************************************/

shared: win32: DEFINES += LUA_BUILD_AS_DLL
linux: DEFINES += LUA_USE_LINUX
gcc: QMAKE_CFLAGS += -Wno-cast-function-type

SOURCES += \
    lua53/lapi.c \
    lua53/lauxlib.c \
    lua53/lbaselib.c \
    lua53/lbitlib.c \
    lua53/lcode.c \
    lua53/lcorolib.c \
    lua53/lctype.c \
    lua53/ldblib.c \
    lua53/ldebug.c \
    lua53/ldo.c \
    lua53/ldump.c \
    lua53/lfunc.c \
    lua53/lgc.c \
    lua53/linit.c \
    lua53/liolib.c \
    lua53/llex.c \
    lua53/lmathlib.c \
    lua53/lmem.c \
    lua53/loadlib.c \
    lua53/lobject.c \
    lua53/lopcodes.c \
    lua53/loslib.c \
    lua53/lparser.c \
    lua53/lstate.c \
    lua53/lstring.c \
    lua53/lstrlib.c \
    lua53/ltable.c \
    lua53/ltablib.c \
    lua53/ltm.c \
    lua53/lundump.c \
    lua53/lutf8lib.c \
    lua53/lvm.c \
    lua53/lzio.c

HEADERS += \
    lua53/lapi.h \
    lua53/lauxlib.h \
    lua53/lcode.h \
    lua53/lctype.h \
    lua53/ldebug.h \
    lua53/ldo.h \
    lua53/lfunc.h \
    lua53/lgc.h \
    lua53/llex.h \
    lua53/llimits.h \
    lua53/lmem.h \
    lua53/lobject.h \
    lua53/lopcodes.h \
    lua53/lparser.h \
    lua53/lprefix.h \
    lua53/lstate.h \
    lua53/lstring.h \
    lua53/ltable.h \
    lua53/ltm.h \
    lua53/lua.h \
    lua53/luaconf.h \
    lua53/lualib.h \
    lua53/lundump.h \
    lua53/lvm.h \
    lua53/lzio.h
