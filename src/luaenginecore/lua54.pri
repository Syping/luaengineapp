#/*****************************************************************************
#* luaEngine Lua Engine for Qt
#* Copyright (C) 2020 Syping
#*
#* Licensed under the Apache License, Version 2.0 (the "License");
#* you may not use this file except in compliance with the License.
#* You may obtain a copy of the License at
#*
#*     http://www.apache.org/licenses/LICENSE-2.0
#*
#* Unless required by applicable law or agreed to in writing, software
#* distributed under the License is distributed on an "AS IS" BASIS,
#* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#* See the License for the specific language governing permissions and
#* limitations under the License.
#*****************************************************************************/

shared: win32: DEFINES += LUA_BUILD_AS_DLL
linux: DEFINES += LUA_USE_LINUX
gcc: QMAKE_CFLAGS += -Wno-cast-function-type

SOURCES += \
    lua54/lapi.c \
    lua54/lauxlib.c \
    lua54/lbaselib.c \
    lua54/lcode.c \
    lua54/lcorolib.c \
    lua54/lctype.c \
    lua54/ldblib.c \
    lua54/ldebug.c \
    lua54/ldo.c \
    lua54/ldump.c \
    lua54/lfunc.c \
    lua54/lgc.c \
    lua54/linit.c \
    lua54/liolib.c \
    lua54/llex.c \
    lua54/lmathlib.c \
    lua54/lmem.c \
    lua54/loadlib.c \
    lua54/lobject.c \
    lua54/lopcodes.c \
    lua54/loslib.c \
    lua54/lparser.c \
    lua54/lstate.c \
    lua54/lstring.c \
    lua54/lstrlib.c \
    lua54/ltable.c \
    lua54/ltablib.c \
    lua54/ltm.c \
    lua54/lundump.c \
    lua54/lutf8lib.c \
    lua54/lvm.c \
    lua54/lzio.c

HEADERS += \
    lua54/lapi.h \
    lua54/lauxlib.h \
    lua54/lcode.h \
    lua54/lctype.h \
    lua54/ldebug.h \
    lua54/ldo.h \
    lua54/lfunc.h \
    lua54/lgc.h \
    lua54/ljumptab.h \
    lua54/llex.h \
    lua54/llimits.h \
    lua54/lmem.h \
    lua54/lobject.h \
    lua54/lopcodes.h \
    lua54/lopnames.h \
    lua54/lparser.h \
    lua54/lprefix.h \
    lua54/lstate.h \
    lua54/lstring.h \
    lua54/ltable.h \
    lua54/ltm.h \
    lua54/lua.h \
    lua54/luaconf.h \
    lua54/lualib.h \
    lua54/lundump.h \
    lua54/lvm.h \
    lua54/lzio.h
