#/*****************************************************************************
#* luaEngine Lua Engine for Qt
#* Copyright (C) 2019-2021 Syping
#*
#* Licensed under the Apache License, Version 2.0 (the "License");
#* you may not use this file except in compliance with the License.
#* You may obtain a copy of the License at
#*
#*     http://www.apache.org/licenses/LICENSE-2.0
#*
#* Unless required by applicable law or agreed to in writing, software
#* distributed under the License is distributed on an "AS IS" BASIS,
#* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#* See the License for the specific language governing permissions and
#* limitations under the License.
#*****************************************************************************/

QT -= gui
TARGET = LuaEngineIO
TEMPLATE = lib
CONFIG += c++11 \
    skip_target_version_ext
VERSION = 0.1

DEFINES += LUAENGINEIO_LIBRARY
static: DEFINES += LUAENGINE_STATIC

# PROJECT PREFIX
unix {
    isEmpty(LUAENGINE_PREFIX): LUAENGINE_PREFIX = /usr/local
    isEmpty(LUAENGINE_LIBDIR): LUAENGINE_LIBDIR = $$LUAENGINE_PREFIX/lib
}

unix: LIBS += "-Wl,-rpath,\'$$LUAENGINE_LIBDIR\'" -L$$OUT_PWD/../luaenginecore -lLuaEngine
CONFIG(debug, debug|release): win32: LIBS += -L$$OUT_PWD/../luaenginecore/debug -lLuaEngine
CONFIG(release, debug|release): win32: LIBS += -L$$OUT_PWD/../luaenginecore/release -lLuaEngine

INCLUDEPATH += \
    ../luaenginecore/luaengine

SOURCES += \
    luaengine/LuaEngineIO.cpp

HEADERS += \
    luaengine/LuaEngineIO.h \
    luaengine/LuaEngineIO_global.h

include(../luaenginecore/lua_module.pri)

unix {
    target.path = $$LUAENGINE_LIBDIR
    INSTALLS += target
}

win32 {
    RC_LANG = 0x0
    QMAKE_TARGET_COMPANY = "Syping"
    QMAKE_TARGET_DESCRIPTION = "LuaEngine I/O Library"
    QMAKE_TARGET_COPYRIGHT = "Copyright (c) 2019-2021 Syping"
    QMAKE_TARGET_PRODUCT = "LuaEngineIO"
}
