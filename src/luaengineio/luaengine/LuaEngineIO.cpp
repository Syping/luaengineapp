/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2019-2021 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#include "LuaEngineIO.h"
#include <QJsonDocument>
#include <QDirIterator>
#include <QJsonObject>
#include <QTextStream>
#include <QJsonArray>
#include <QJsonValue>
#include <QFileInfo>
#include <QString>
#include <QObject>
#include <QFile>
#ifdef Q_OS_WIN
#include "windows.h"
#include <iostream>
#else
#include "unistd.h"
#endif

void LuaEngineIO::pushClass(lua_State *L_p)
{
    // Directory
    pushFunction(L_p, "checkDirectoryExists", checkDirectoryExists);
    pushFunction(L_p, "getDirectoryContent", getDirectoryContent);
    pushFunction(L_p, "getDirectoryPath", getDirectoryPath);
    pushVariant(L_p, "Files", (int)1);
    pushVariant(L_p, "Directories", (int)2);
    pushVariant(L_p, "Subdirectories", (int)4);

    // Engine IO
    pushFunction(L_p, "eIOBytesAvailable", eIOBytesAvailable);
    pushFunction(L_p, "eIOCanReadLine", eIOCanReadLine);
    pushFunction(L_p, "eIORead", eIORead);
    pushFunction(L_p, "eIOReadAll", eIOReadAll);
    pushFunction(L_p, "eIOReadLine", eIOReadLine);
    pushFunction(L_p, "eIOWrite", eIOWrite);

    // File
    pushFunction(L_p, "checkFileExists", checkFileExists);
    pushFunction(L_p, "linkFile", linkFile);
    pushVariant(L_p, "Symlink", (int)0);
    pushVariant(L_p, "Hardlink", (int)1);

    // Json
    pushFunction(L_p, "jsonToTable", jsonToTable);
    pushFunction(L_p, "tableToJson", tableToJson);
    pushVariant(L_p, "JsonCompact", (int)QJsonDocument::Compact);
    pushVariant(L_p, "JsonIndented", (int)QJsonDocument::Indented);
}

void LuaEngineIO::pushClass(LuaEngine *luaEngine)
{
    pushClass(luaEngine->luaState());
}

int LuaEngineIO::checkDirectoryExists(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 1) {
        QFileInfo fileInfo(getVariant(L_p, 1).toString());
        if (fileInfo.exists() && fileInfo.isDir()) {
            pushVariant(L_p, true);
            return 1;
        }
    }
    pushVariant(L_p, false);
    return 1;
}

int LuaEngineIO::checkFileExists(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 1) {
        QFileInfo fileInfo(getVariant(L_p, 1).toString());
        if (fileInfo.exists() && fileInfo.isFile()) {
            pushVariant(L_p, true);
            return 1;
        }
    }
    pushVariant(L_p, false);
    return 1;
}

int LuaEngineIO::eIOBytesAvailable(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 1) {
        void *pointer = getPointer(L_p, 1);
        if (pointer != NULL) {
            if (((QObject*)pointer)->inherits("QIODevice")) {
                pushVariant(L_p, ((QIODevice*)pointer)->bytesAvailable());
                return 1;
            }
        }
    }
    return 0;
}

int LuaEngineIO::eIOCanReadLine(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 1) {
        void *pointer = getPointer(L_p, 1);
        if (pointer != NULL) {
            if (((QObject*)pointer)->inherits("QIODevice")) {
                pushVariant(L_p, ((QIODevice*)pointer)->canReadLine());
                return 1;
            }
        }
    }
    return 0;
}

int LuaEngineIO::eIORead(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 2) {
        void *pointer = getPointer(L_p, 1);
        if (pointer != NULL) {
            if (((QObject*)pointer)->inherits("QIODevice")) {
                const QByteArray readData = ((QIODevice*)pointer)->read(getVariant(L_p, 2).toLongLong());
                pushVariant(L_p, QString::fromUtf8(readData));
                return 1;
            }
        }
    }
    return 0;
}

int LuaEngineIO::eIOReadAll(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 1) {
        void *pointer = getPointer(L_p, 1);
        if (pointer != NULL) {
            if (((QObject*)pointer)->inherits("QIODevice")) {
                const QByteArray readData = ((QIODevice*)pointer)->readAll();
                pushVariant(L_p, QString::fromUtf8(readData));
                return 1;
            }
        }
    }
    return 0;
}

int LuaEngineIO::eIOReadLine(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 1) {
        void *pointer = getPointer(L_p, 1);
        if (pointer != NULL) {
            if (((QObject*)pointer)->inherits("QIODevice")) {
                const QByteArray readData = ((QIODevice*)pointer)->readLine();
                pushVariant(L_p, QString::fromUtf8(readData));
                return 1;
            }
        }
    }
    return 0;
}

int LuaEngineIO::eIOWrite(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 2) {
        void *pointer = getPointer(L_p, 1);
        if (pointer != NULL) {
            if (((QObject*)pointer)->inherits("QIODevice")) {
                ((QIODevice*)pointer)->write(getVariant(L_p, 2).toString().toUtf8());
            }
        }
    }
    return 0;
}

int LuaEngineIO::getDirectoryContent(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 1) {
        QStringList directories;
        QStringList nameFilters;
        QString directory = getVariant(L_p, 1).toString();
        QDir::Filters dirFilter = QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot;
        QDirIterator::IteratorFlag dirIteratorFlag = QDirIterator::NoIteratorFlags;
        if (getArgumentCount(L_p) >= 2) {
            QVariant filterVariant = getVariant(L_p, 2);
            if ((QMetaType::Type)filterVariant.type() == QMetaType::QVariantMap) {
                QVariantMap filterMap = filterVariant.toMap();
                QVariantMap::const_iterator it = filterMap.constBegin();
                QVariantMap::const_iterator end = filterMap.constEnd();
                while (it != end) {
                    nameFilters << it.value().toString();
                    it++;
                }
            }
            else {
                QString filterString = filterVariant.toString();
                if (!filterString.isEmpty()) {
                    nameFilters << filterVariant.toString();
                }
            }
            if (getArgumentCount(L_p) >= 3) {
                bool ok;
                int filterInt = getVariant(L_p, 3).toInt(&ok);
                if (ok) {
                    switch (filterInt) {
                    case 1:
                        dirFilter = QDir::Files | QDir::NoDotAndDotDot;
                        break;
                    case 2:
                        dirFilter = QDir::Dirs | QDir::NoDotAndDotDot;
                        break;
                    case 4:
                    case 7:
                        dirIteratorFlag = QDirIterator::Subdirectories;
                        break;
                    case 5:
                        dirFilter = QDir::Files | QDir::NoDotAndDotDot;
                        dirIteratorFlag = QDirIterator::Subdirectories;
                        break;
                    case 6:
                        dirFilter = QDir::Dirs | QDir::NoDotAndDotDot;
                        dirIteratorFlag = QDirIterator::Subdirectories;
                        break;
                    }
                }
            }
        }
        QDirIterator dirIterator(directory, nameFilters, dirFilter, dirIteratorFlag);
        while (dirIterator.hasNext()) {
            directories << dirIterator.next();
        }
        pushVariant(L_p, directories);
        return 1;
    }
    return 0;
}

int LuaEngineIO::getDirectoryPath(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 1) {
        QFileInfo fileInfo(getVariant(L_p, 1).toString());
        if (fileInfo.exists()) {
            pushVariant(L_p, fileInfo.absoluteDir().absolutePath());
            return 1;
        }
    }
    return 0;
}

int LuaEngineIO::linkFile(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 2) {
        bool symlinkMode = true;
        if (getArgumentCount(L_p) >= 3 && getVariant(L_p, 3).toInt() == 1) {
            symlinkMode = false;
        }
        bool linkSucceeded = false;
        if (symlinkMode) {
#ifdef Q_OS_WIN
            QString targetFile = getVariant(L_p, 2).toString();
            if (targetFile.right(4) != ".lnk") {
                targetFile += ".lnk";
            }
            linkSucceeded = QFile::link(getVariant(L_p, 1).toString(), targetFile);
#else
            linkSucceeded = QFile::link(getVariant(L_p, 1).toString(), getVariant(L_p, 2).toString());
#endif
        }
        else {
#ifdef Q_OS_WIN
            linkSucceeded = CreateHardLinkW(getVariant(L_p, 2).toString().toStdWString().c_str(), getVariant(L_p, 1).toString().toStdWString().c_str(), NULL);
#else
            linkSucceeded = link(getVariant(L_p, 1).toString().toUtf8().data(), getVariant(L_p, 2).toString().toUtf8().data());
#endif
        }
        pushVariant(L_p, linkSucceeded);
        return 1;
    }
    getVariant(L_p, false);
    return 1;
}

int LuaEngineIO::jsonToTable(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 1) {
        const QJsonDocument jsonDocument = QJsonDocument::fromJson(getVariant(L_p, 1).toString().toUtf8());
        if (jsonDocument.isObject()) {
            pushVariant(L_p, jsonDocument.object().toVariantMap());
            return 1;
        }
        else if (jsonDocument.isArray()) {
            pushVariant(L_p, jsonDocument.array().toVariantList());
            return 1;
        }
    }
    return 0;
}

int LuaEngineIO::tableToJson(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 1) {
        QJsonDocument::JsonFormat jsonFormat = QJsonDocument::Compact;
        if (getArgumentCount(L_p) >= 2) {
            jsonFormat = static_cast<QJsonDocument::JsonFormat>(getVariant(L_p, 2).toInt());
        }
        pushVariant(L_p, QString::fromUtf8(QJsonDocument(QJsonObject::fromVariantMap(getVariant(L_p, 1).toMap())).toJson(jsonFormat)));
        return 1;
    }
    return 0;
}

QString LuaEngineIO::nameForPointer(void *pointer)
{
    QString nameStorage;
    QTextStream(&nameStorage) << "LuaEngineIO" << pointer;
    return nameStorage;
}
