/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2019 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#ifndef LUAENGINEIO_H
#define LUAENGINEIO_H

#include "LuaEngineIO_global.h"
#include "LuaEngineAddon.h"
#include "LuaEngine.h"
#include <QString>
#include <QObject>

class LUAENGINEIOSHARED_EXPORT LuaEngineIO : public LuaEngineAddon
{
    Q_OBJECT
public:
    static void pushClass(lua_State *L_p);
    static void pushClass(LuaEngine *luaEngine);
    static int checkDirectoryExists(lua_State *L_p);
    static int checkFileExists(lua_State *L_p);
    static int eIOBytesAvailable(lua_State *L_p);
    static int eIOCanReadLine(lua_State *L_p);
    static int eIORead(lua_State *L_p);
    static int eIOReadAll(lua_State *L_p);
    static int eIOReadLine(lua_State *L_p);
    static int eIOWrite(lua_State *L_p);
    static int getDirectoryContent(lua_State *L_p);
    static int getDirectoryPath(lua_State *L_p);
    static int linkFile(lua_State *L_p);
    static int jsonToTable(lua_State *L_p);
    static int tableToJson(lua_State *L_p);

private:
    lua_State *L;
    static QString nameForPointer(void *pointer);
};

#endif // LUAENGINEIO_H
