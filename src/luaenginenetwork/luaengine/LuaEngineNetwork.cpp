/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2020 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#include "LuaEngineNetwork.h"
#include <QAbstractSocket>
#include <QLocalSocket>
#ifdef QT_NO_SSL
#include <QTcpSocket>
#else
#include <QSslSocket>
#endif

void LuaEngineNetwork::pushClass(lua_State *L_p)
{
    // Local Socket
    pushFunction(L_p, "createLocalSocket", createLocalSocket);

    // Socket
    pushFunction(L_p, "socketConnect", socketConnect);
    pushFunction(L_p, "socketDisconnect", socketDisconnect);
    pushFunction(L_p, "socketFlush", socketFlush);

    // TCP Socket
    pushFunction(L_p, "createTcpSocket", createTcpSocket);
    pushFunction(L_p, "tcpSocketStartTLS", tcpSocketStartTLS);

    // TLS
    pushFunction(L_p, "luaEngineTLSAvailable", luaEngineTLSAvailable);
}

void LuaEngineNetwork::pushClass(LuaEngine *luaEngine)
{
    pushClass(luaEngine->luaState());
}

int LuaEngineNetwork::createLocalSocket(lua_State *L_p)
{
    QObject *parent = nullptr;
    if (getArgumentCount(L_p) >= 1) {
        void *pointer = getPointer(L_p, 1);
        if (pointer != NULL) {
            parent = (QObject*)pointer;
        }
    }
    QLocalSocket *localSocket = new QLocalSocket(parent);
    localSocket->setObjectName(nameForPointer(localSocket));
    pushPointer(L_p, localSocket);
    return 1;
}

int LuaEngineNetwork::createTcpSocket(lua_State *L_p)
{
    QObject *parent = nullptr;
    if (getArgumentCount(L_p) >= 1) {
        void *pointer = getPointer(L_p, 1);
        if (pointer != NULL) {
            parent = (QObject*)pointer;
        }
    }
#ifdef QT_NO_SSL
    QTcpSocket *tcpSocket = new QTcpSocket(parent);
#else
    QTcpSocket *tcpSocket;
    if (QSslSocket::supportsSsl()) {
        tcpSocket = new QSslSocket(parent);
    }
    else {
        tcpSocket = new QTcpSocket(parent);
    }
#endif
    tcpSocket->setObjectName(nameForPointer(tcpSocket));
    pushPointer(L_p, tcpSocket);
    return 1;
}

int LuaEngineNetwork::luaEngineTLSAvailable(lua_State *L_p)
{
#ifdef QT_NO_SSL
    pushVariant(L_p, false);
#else
    pushVariant(L_p, QSslSocket::supportsSsl());
#endif
    return 1;
}

int LuaEngineNetwork::socketConnect(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 2) {
        void *pointer = getPointer(L_p, 1);
        if (pointer != NULL) {
            if (((QObject*)pointer)->inherits("QLocalSocket")) {
                ((QLocalSocket*)pointer)->connectToServer(getVariant(L_p, 2).toString(), QIODevice::ReadWrite);
            }
            else if (((QObject*)pointer)->inherits("QAbstractSocket")) {
                const QStringList remoteIPPort = getVariant(L_p, 2).toString().split(":");
                if (remoteIPPort.length() >= 2) {
                    ((QAbstractSocket*)pointer)->connectToHost(remoteIPPort.at(0), remoteIPPort.at(1).toUShort(), QIODevice::ReadWrite);
                }
            }
        }
    }
    return 0;
}

int LuaEngineNetwork::socketDisconnect(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 1) {
        void *pointer = getPointer(L_p, 1);
        if (pointer != NULL) {
            if (((QObject*)pointer)->inherits("QLocalSocket")) {
                ((QLocalSocket*)pointer)->disconnectFromServer();
            }
            else if (((QObject*)pointer)->inherits("QAbstractSocket")) {
                ((QAbstractSocket*)pointer)->disconnectFromHost();
            }
        }
    }
    return 0;
}

int LuaEngineNetwork::socketFlush(lua_State *L_p)
{
    if (getArgumentCount(L_p) >= 1) {
        void *pointer = getPointer(L_p, 1);
        if (pointer != NULL) {
            if (((QObject*)pointer)->inherits("QAbstractSocket")) {
                bool isFlushed = ((QAbstractSocket*)pointer)->flush();
                pushVariant(L_p, isFlushed);
                return 1;
            }
        }
    }
    return 0;
}

int LuaEngineNetwork::tcpSocketStartTLS(lua_State *L_p)
{
#ifndef QT_NO_SSL
    if (getArgumentCount(L_p) >= 1) {
        void *pointer = getPointer(L_p, 1);
        if (pointer != NULL) {
            if (((QObject*)pointer)->inherits("QSslSocket")) {
                ((QSslSocket*)pointer)->startClientEncryption();
                pushVariant(L_p, true);
                return 1;
            }
        }
    }
#endif
    pushVariant(L_p, false);
    return 1;
}

QString LuaEngineNetwork::nameForPointer(void *pointer)
{
    QString nameStorage;
    QTextStream(&nameStorage) << "LuaEngineNetwork" << pointer;
    return nameStorage;
}
