/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2020 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#ifndef LUAENGINENETWORK_H
#define LUAENGINENETWORK_H

#include "LuaEngineNetwork_global.h"
#include "LuaEngineAddon.h"
#include "LuaEngine.h"

class LUAENGINENETWORKSHARED_EXPORT LuaEngineNetwork : public LuaEngineAddon
{
    Q_OBJECT
public:
    static void pushClass(lua_State *L_p);
    static void pushClass(LuaEngine *luaEngine);
    static int createLocalSocket(lua_State *L_p);
    static int createTcpSocket(lua_State *L_p);
    static int luaEngineTLSAvailable(lua_State *L_p);
    static int socketConnect(lua_State *L_p);
    static int socketDisconnect(lua_State *L_p);
    static int socketFlush(lua_State *L_p);
    static int tcpSocketStartTLS(lua_State *L_p);

private:
    lua_State *L;
    static QString nameForPointer(void *pointer);
};

#endif // LUAENGINENETWORK_H
