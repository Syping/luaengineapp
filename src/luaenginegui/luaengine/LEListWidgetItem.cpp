/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2020 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#include "LEListWidgetItem.h"

LEListWidgetItem::LEListWidgetItem(const QString &text, QListWidget *parent)
{
    item_p.setText(text);
    parent->addItem(&item_p);
    QObject::connect(parent, &QListWidget::currentItemChanged, this, &LEListWidgetItem::listWidgetItemChanged);
}

QListWidgetItem* LEListWidgetItem::item()
{
    return &item_p;
}

void LEListWidgetItem::listWidgetItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    Q_UNUSED(previous)
    if (current == &item_p) {
        emit selected();
    }
}
