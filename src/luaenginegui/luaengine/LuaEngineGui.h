/*****************************************************************************
* luaEngine Lua Engine for Qt
* Copyright (C) 2018-2021 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*****************************************************************************/

#ifndef LUAENGINEGUI_H
#define LUAENGINEGUI_H

#include "LuaEngineGui_global.h"
#include "LuaEngine.h"
#include <QWidget>
#include <QLayout>
#include <QObject>
#include <QString>

class LUAENGINEGUISHARED_EXPORT LuaEngineGui : public LuaEngine
{
    Q_OBJECT
public:
    LuaEngineGui(QObject *parent = nullptr, bool loadBaseLibraries = true);
    LuaEngineGui(LuaEngineType engineType, QObject *parent = nullptr, bool loadBaseLibraries = true);
    static void pushClass(lua_State *L_p);
    static void pushClass(LuaEngine *luaEngine);
    static int updateUi(lua_State *L_p);
    static int showFileDialog(lua_State *L_p);
    static int showMessageBox(lua_State *L_p);
    static int closeWidget(lua_State *L_p);
    static int executeWidget(lua_State *L_p);
    static int showWidget(lua_State *L_p);
    static int setCurrentStack(lua_State *L_p);
    static int setLayoutMargins(lua_State *L_p);
    static int setMenuShortcut(lua_State *L_p);
    static int setObjectImage(lua_State *L_p);
    static int setObjectText(lua_State *L_p);
    static int setWidgetChecked(lua_State *L_p);
    static int setWidgetEnabled(lua_State *L_p);
    static int setWidgetFixed(lua_State *L_p);
    static int setWidgetImageSize(lua_State *L_p);
    static int setWidgetLayout(lua_State *L_p);
    static int setWidgetMaximum(lua_State *L_p);
    static int setWidgetMinimum(lua_State *L_p);
    static int setWidgetParent(lua_State *L_p);
    static int setWidgetReadOnly(lua_State *L_p);
    static int setWidgetSize(lua_State *L_p);
    static int setWidgetSizePolicy(lua_State *L_p);
    static int setWidgetValue(lua_State *L_p);
    static int setWidgetVisible(lua_State *L_p);
    static int layoutAddLayout(lua_State *L_p);
    static int layoutAddWidget(lua_State *L_p);
    static int widgetAddText(lua_State *L_p);
    static int addWidgetAsStack(lua_State *L_p);
    static int addWidgetAsTab(lua_State *L_p);
    static int createCentralWidget(lua_State *L_p);
    static int createCheckBox(lua_State *L_p);
    static int createDialog(lua_State *L_p);
    static int createGroupBox(lua_State *L_p);
    static int createLabel(lua_State *L_p);
    static int createLayout(lua_State *L_p);
    static int createLineEdit(lua_State *L_p);
    static int createListItem(lua_State *L_p);
    static int createListView(lua_State *L_p);
    static int createMainWindow(lua_State *L_p);
    static int createMenu(lua_State *L_p);
    static int createMenuBar(lua_State *L_p);
    static int createMenuEntry(lua_State *L_p);
    static int createMenuSeparator(lua_State *L_p);
    static int createPlainTextEdit(lua_State *L_p);
    static int createProgressBar(lua_State *L_p);
    static int createPushButton(lua_State *L_p);
    static int createRadioButton(lua_State *L_p);
    static int createSpacerItem(lua_State *L_p);
    static int createStackSwitch(lua_State *L_p);
    static int createTabBar(lua_State *L_p);
    static int createTextEdit(lua_State *L_p);
    static int createToolBar(lua_State *L_p);
    static int createToolButton(lua_State *L_p);
    static int createWidgetTab(lua_State *L_p);
    static int createWidgetStack(lua_State *L_p);
    static int isWidgetChecked(lua_State *L_p);
    static int isWidgetEnabled(lua_State *L_p);
    static int isWidgetVisible(lua_State *L_p);
    static int getObjectText(lua_State *L_p);
    static int getObjectWindow(lua_State *L_p);
    static int getWidgetPixelRatio(lua_State *L_p);

private:
    static QWidget* windowForObject(QObject *object);
    static QString pathForString(const QString &string, LuaEngine *engine);
    static void lpForPointer(void *pointer, QLayout **layout, QWidget **parent);
    static QString nameForPointer(void *pointer);
};

#endif // LUAENGINEGUI_H
