#/*****************************************************************************
#* luaEngine Lua Engine for Qt
#* Copyright (C) 2018-2020 Syping
#*
#* Licensed under the Apache License, Version 2.0 (the "License");
#* you may not use this file except in compliance with the License.
#* You may obtain a copy of the License at
#*
#*     http://www.apache.org/licenses/LICENSE-2.0
#*
#* Unless required by applicable law or agreed to in writing, software
#* distributed under the License is distributed on an "AS IS" BASIS,
#* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#* See the License for the specific language governing permissions and
#* limitations under the License.
#*****************************************************************************/

TEMPLATE = subdirs

SUBDIRS += luaenginecore \
    luaenginegui \
    luaenginenetwork \
    luaengineio \
    luaengineos

luaenginegui.depends = luaenginecore
luaenginenetwork.depends = luaenginecore
luaengineio.depends = luaenginecore
luaengineos.depends = luaenginecore

luaengine.depends += luaenginecore \
    luaenginegui
    luaenginenetwork
    luaengineio
    luaengineos

luaenginecli.depends += luaenginecore \
    luaenginenetwork
    luaengineio
    luaengineos

luaengineapp.depends += luaenginecore \
    luaenginegui
    luaenginenetwork
    luaengineio
    luaengineos

luaenginec.depends = luaenginecore

CONFIG(WITH_LUAENGINE_RUNTIME): SUBDIRS += luaengine
CONFIG(WITH_LUAENGINE_RUNTIME): SUBDIRS += luaenginecli
CONFIG(WITH_LUAENGINE_APPSTUB): SUBDIRS += luaengineapp
CONFIG(WITH_LUAENGINE_COMPILER): SUBDIRS += luaenginec
